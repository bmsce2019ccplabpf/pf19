#include<stdio.h>
int swap(int*m, int*n);
int main()
{
int m,n;
printf("enter m & n\n");
scanf("%d%d",&m,&n);
swap(&m,&n);
printf("The values of m and n after swapping are");
printf("n=%d m=%d",m,n);
return 0;
}
int swap(int*m, int*n)
{
int t;
t=*m;
*m=*n;
*n=t;
return t;
}